namespace hide

universe u

constant list : Type u → Type u

constant cons : Π α : Type u, α → list α → list α
constant nil : Π α : Type u, list α

#check cons

end hide

-- Π x : α, β x
-- for a function f of this type we have that
-- for each x ∈ α we have (f a) ∈ β x
